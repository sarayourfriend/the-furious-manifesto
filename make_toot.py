import os
import dotenv
from mastodon import Mastodon
from generate_toot import generate_toot

dotenv.load_dotenv()


def make_toot(base_url, api_key):
    mastodon = Mastodon(
        access_token=api_key,
        api_base_url=base_url
    )

    with open('./corpus.txt', 'r') as f:
        corpus = f.read()

    toot = generate_toot(corpus)

    mastodon.toot(toot)


if __name__ == '__main__':
    base_url = os.getenv('MASTODON_BASE_URL')
    api_key = os.getenv('MASTODON_API_KEY')
    make_toot(base_url, api_key)
