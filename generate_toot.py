import markovify
import argparse
import string

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input', dest='input', help="Input file corups")

args = parser.parse_args()


printable = set(string.printable)


def generate_toot(corpus):
    text_model = markovify.Text(corpus)

    sentence = text_model.make_sentence()
    return ''.join([c for c in sentence if c in printable])


if __name__ == '__main__':
    with open(args.input, 'r') as f:
        corpus = f.read()

    sentence = generate_toot(corpus)

    print(sentence)
