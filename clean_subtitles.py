import argparse
import os

parser = argparse.ArgumentParser(
    description="Clean a .srt to get the sentences said by the narrator")
parser.add_argument('-i', '--input', dest='input',
                    help="The path to the SRT file")
parser.add_argument('-o', '--out', dest='output',
                    help="The path to the output file")

args = parser.parse_args()


def is_time_range_line(line):
    # This is clearly naive but like... david attenborough
    # isn't saying 'hyphen hyphen greater than sign'
    return ' --> ' in line


def is_number_line(line):
    try:
        int(line)
        return True
    except ValueError:
        return False


if __name__ == '__main__':
    if not (args.input and args.output):
        print('please provide both the input and output files')
        exit(1)

    with open(args.input, encoding="utf8", errors='ignore') as srt:
        real_lines = []
        sentence = []
        for line in srt:
            # subtitle files are messy y'all... 🙄
            line = line.replace('\x00', '').rstrip()

            if is_number_line(line) or is_time_range_line(line) or len(line) == 0:
                # we can throw these lines away
                continue

            real_lines.append(line)

    corpus = ' '.join(real_lines)

    with open(args.output, 'w') as output:
        output.write(corpus)

    print('done!')
