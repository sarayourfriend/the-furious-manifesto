import os

corpus = []
paths = os.listdir('./subtitles')
for path in paths:
    with open('./subtitles/'+path, 'r') as file:
        for line in file:
            corpus.append(line)

with open('./corpus.txt', 'w') as output:
    output.write(' '.join(corpus))
